; We are loaded by BIOS at 0x7C00
org     0x7c00
 
; We are still in 16 bit Real Mode
bits    16
 
Start:
 
    ; Clear all Interrupts
    ;cli
    ; halt the system
    hlt
    
; We have to be 512 bytes. Clear the rest of the bytes with 0
times 510 - ($-$$) db 0
 
; Boot Signiture
dw 0xAA55
