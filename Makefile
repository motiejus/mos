floppy.img: boot1.bin
	dd if=/dev/zero of=$@ bs=512 count=2880
	dd conv=notrunc if=$< of=$@

boot1.bin: boot1.asm
	nasm -f bin $< -o $@
